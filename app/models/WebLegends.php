<?php

class WebLegends extends Eloquent{
    protected $table = 'web_legends';
    public $timestamps = false;

    static public function saveLegend(){
        if(isset($_FILES['photo']['tmp_name'])){
            Image::make(
                Input::file('photo')->getRealPath()
            )->resize(160, 160)
             ->save('img/legendary/'.Input::get('name').'.jpg');
        }else{
            return false;
        }

        $legend = new WebLegends();
        $legend->name = Input::get('name');
        $legend->type = Input::get('type');
        $legend->link = Input::get('link');
        $legend->description = Input::get('description');
        $legend->logo = Input::get('name').'.jpg';
        $legend->save();
    }
}