<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'UnrealPagesController@actionHome');

/**
 * Основные страницы
 */
//История развития проекта
Route::get('/history', 'UnrealPagesController@actionHistory');
//О проекте
Route::get('/about', 'UnrealPagesController@actionAbout');
//Контакты
Route::get('/contacts', 'UnrealPagesController@actionContacts');
//Наши проекты
Route::get('/news', 'UnrealPagesController@actionNews');
//Подпроект web-легенды
Route::get('/legends', 'WebLegendsController@actionBigLegends');


Route::get('/sites/{name}', 'UnrealPagesController@actionSites');
Route::get('/portfolio', 'UnrealPagesController@actionPortfolio');



Route::get('/services/{name}', 'UnrealPagesController@actionServices');


Route::get('/sitemap.xml', 'SitemapController@actionSitemap');


/**
 * Админка
 */

Route::get('/login', 'RegController@actionLogin');
Route::get('/logout', 'RegController@actionLogout');
Route::post('/login', 'RegController@actionAuth');
Route::when('admin/*', 'admin');

Route::any('/admin/legends', 'WebLegendsController@actionAddLegends');

Route::any('/admin/legends_delete', 'WebLegendsController@actionDeleteLegends');


/**
 * На почту
 */
Route::post('/mail/orders_services', 'UnrealMailController@actionOrderServices');

//Ошибки
//Route::get('/errors', 'UnrealPagesController@actionErrors');

/*
 * Редиректы
 */

//Route::get('/index.php', function(){
//    return Redirect::to('/');
//});

Route::get('/services', function(){
    return Redirect::to('/services/development');
});

Route::get('/sites', function(){
    return Redirect::to('/sites/azovsky');
});

