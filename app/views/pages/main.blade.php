@extends('layouts.master')

@section('head')
@endsection

@section('content')
<div class="header"><h1>Веб-студия</h1><span>Реально нереальные проекты!</span></div>
<div class="services block">
    <div class="service_full">
        <div>
<!--            <img src="/img/unreal-logo.jpg" alt="Unreal Projects"/>-->
<!--            <img src="/img/services/create.jpg" class="service_second_img" alt="Создание сайта"/>-->
<!--            <img src="/img/black/front-logo.jpg" alt="Unreal Projects"/>-->
            <img src="/img/black/front-logo_big.png" alt="Unreal Projects"/>
        </div>

        <div class="row_sites">
            <div class="block_round">
                <h2 class="name_round">Сайт <br />визитка</h2>
                <div class="icon_round"><img src="/img/services/icon_visit.png"/></div>
                <p class="description_round">Создание веб-лица компании, автора или продукта:
                                             презентация, landing-page, сайт-визитка.
                </p>
            </div>

            <div class="block_round">
                <h2 class="name_round">Информационный <br>портал</h2>
                <div class="icon_round"><img src="/img/services/icon_portal.png"/></div>
                <p class="description_round">Создание тематического ресурса для веб-сообщества:
                                             информационный портал, социальная сеть, коллективный блог.
                </p>
            </div>

            <div class="block_round">
                <h2 class="name_round">Интернет <br>магазин</h2>
                <div class="icon_round"><img src="/img/services/icon_market.png"/></div>
                <p class="description_round">Создание веб-портала для электронной комерции:
                                             интернет-магазин, веб-каталог, банкинг.
                </p>
            </div>

            <div class="block_round">
                <h2 class="name_round">Нереальный <br>проект</h2>
                <div class="icon_round"><img src="/img/cube/cube_150.gif"/></div>
                <p class="description_round">Реализация нестандартных проектов любой сложности и под любые цели.</p>
            </div>
        </div>

<!--        <div class="service">-->
<!--            <h2><a href="/services/development/">Создание сайта</a></h2>-->
<!--            <ul>-->
<!--                <li><span></span>Создание сайта визитки</li>-->
<!--                <li><span></span>Создание информационного портала</li>-->
<!--                <li><span></span>Создание интернет магазина</li>-->
<!--                <li><span></span>Создание любого web-проекта</li>-->
<!--            </ul>-->
<!--        </div>-->

    </div>

    <div class="service">
        <img src="/img/services/remade.jpg" alt="Улучшение сайта"/>
        <h2><a href="/services/improvement/">Улучшение сайта</a></h2>
        <ul>
            <li><span></span>Расширение функционала сайта</li>
            <li><span></span>Добавление тематических страниц на сайт</li>
            <li><span></span>Разработка компонентов и модулей</li>
            <li><span></span>Редизайн сайта</li>
        </ul>
    </div>

    <div class="service">
        <img src="/img/services/tech.jpg" alt="Техническое обслуживание сайта"/>
        <h2><a href="/services/servicing/">Починка сайта</a></h2>
        <ul>
            <li><span></span>Исправление ошибок в работе сайта</li>
            <li><span></span>Техническое обслуживание сайта</li>
            <li><span></span>Восстановление сайта после взлома</li>
            <li><span></span>Оптимизация сайта</li>
        </ul>
    </div>

    <div class="service">
        <img src="/img/services/content.jpg" alt="Контент сайта"/>
        <h2><a href="/services/content/">Контент сайта</a></h2>
        <ul>
            <li><span></span>Написание и рерайт статей</li>
            <li><span></span>Веедение информационных сайтов</li>
            <li><span></span>Наполнение сайта информацией</li>
            <li><span></span>Обновление информации на сайте</li>
        </ul>
    </div>

    <div class="service">
        <img src="/img/services/promote.jpg" alt="Контент сайта"/>
        <h2><a href="/services/promotion/">Продвижение сайта</a></h2>
        <ul>
            <li><span></span>Продвижение бренда</li>
            <li><span></span>Social media marketing (SMM)</li>
            <li><span></span>Search engine optimization (SEO)</li>
            <li><span></span>Public Relations (PR)</li>
        </ul>
    </div>
</div>

<div class="header"><h2>Навигация по сайту</h2><span>Что у нас интересного?</span></div>
<div class="services block">
    <div class="service menu_block_services">
        <h2>Услуги</h2>
        <ul>
            <a href="/services/development"><li><span></span>Разработка</li></a>
            <a href="/services/improvement"><li><span></span>Улучшение</li></a>
            <a href="/services/servicing"><li><span></span>Починка</li></a>
            <a href="/services/content"><li><span></span>Наполнение</li></a>
            <a href="/services/promotion"><li><span></span>Продвижение</li></a>
        </ul>
    </div>

    <div class="service">
        <h2>Оставить заявку</h2>
        <form id="order_services_front">
            <input type="text" autocomplete="off" name="name" placeholder="ВАШЕ ИМЯ">
            <input type="text" autocomplete="off" name="phone" placeholder="ВАШ ТЕЛЕФОН">
            <input type="text" autocomplete="off" name="mail" placeholder="ВАШ E-MAIL">
            <textarea name="task" placeholder="ВАША ЗАДАЧА"></textarea>
            <input type="submit" value="ОТПРАВИТЬ ЗАЯВКУ">
        </form>
    </div>


    <div class="service">
        <h2>Компания</h2>
        <ul>
            <a href="/about"><li><span></span>О нас</li></a>
            {{--<a href="/news"><li><span></span>Новости</li></a>
            <a href="/history"><li><span></span>История</li></a>
            <a href="/legends"><li><span></span>web легенды</li></a>--}}
            <a href="/contacts"><li><span></span>Контакты</li></a>
        </ul>
    </div>

            <div class="service">
                <p>
                    Создание веб-сайтов с нуля - это приоритет нашей компании.
                    Мы создаем сайты любой направлености, сложности и функциональности.
                    Сайт-визитка, интернет-магазин, информационный портал или "сайт под ключ" - мы создадим любой проект.
                    С компанией Unreal Projects Ваши идеи реализуются максимально быстро и качественно.
                </p>
            </div>
</div>

<div class="header"><h2>Контакты</h2><span>Пишите нам на email и звоните в skype</span></div>

<div class='main_contacts default_page_block block'>
    <div class="contacts_list">
        <div class="skype" alt="Скайп"><b>unreal-projects</b><span class="icons"></span></div>
        <div class="mail"><span class="icons"></span><b alt="Почта">office@unrealprojects.com</b></div>
    </div>
</div>
@endsection