@extends('layouts.master')

@section('head')
@endsection

@section('content')
<div class="header"><h1>Контакты</h1><span>Мы всегда готовы с вами общаться</span></div>

<div class="services block">
    <div class="services_line">
        <div class="inner_services">
            <img src="/img/unreal-logo.jpg" alt="Невероятные web проекты"/>
        </div>

        <div class="inner_content">
            <h2 style="padding-bottom: 7px;">Оставить заявку</h2>
            <form id="order_services_contacts">
                <input type="text" autocomplete="off" name="name" placeholder="ВАШЕ ИМЯ">
                <input type="text" autocomplete="off" name="phone" placeholder="ВАШ ТЕЛЕФОН">
                <input type="text" autocomplete="off" name="mail" placeholder="ВАШ E-MAIL">
                <textarea name="task" placeholder="ВАША ЗАДАЧА"></textarea>
                <input type="submit" value="ОТПРАВИТЬ ЗАЯВКУ">
            </form>

        </div>
    </div>
    <div class="service service_inner_ul">
        <h2>Как с нами связаться?</h2>
        <ul>
            <li><span></span>Позвонить в skype: <i>unreal-projects</i></li>
            <li><span></span>Написать на почту <i>office@unrealprojects.com</i></li>
            <li><span></span>Оставить заявку на сайт</li>
        </ul>
    </div>


    <div class="service service_inner_ul">
        <p>
            Если Вас заинтересовали наши услуги, либо Вам нужна консультация
            или же вы просто хотите пообщаться с интересными людьми -
            пишите нам на нашу почту (<a href="mailto:office@unrealprojects.com">office@unrealprojects.com</a>) или в скайп (unrealprojects). Всегда будем рады!
        </p>
    </div>

</div>
@endsection

