@extends('layouts.master')

@section('head')
<link type="text/css" rel="stylesheet" href="css/history.css">
@endsection

@section('content')
<div class='history_coll'>
    @foreach($events as $event)
    <div class='event'>
        <div class="event_name">{{$event['event']}}</div>
        <div class="event_description">{{$event['description']}}</div>
        <div class="event_data">{{$event['data']}}</div>
        <div class="history_line">
            <div class="dot_left"></div>
            <div class="line"></div>
            <div class="dot_right"></div>
            <div class="year">{{substr($event['data'],0,4)}}</div>
        </div>
    </div>

    @endforeach
</div>
@endsection