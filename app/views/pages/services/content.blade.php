@extends('layouts.master')

@section('head')
@endsection

@section('content')
<div class="header"><h1>Наполнение сайта</h1><span>Все что касается сайтов - это к нам</span></div>

<div class="services block">
    <div class="services_line">
        <div class="inner_services">
            <img src="/img/services/content.jpg" alt="Наполнение сайта"/>
        </div>

        <div class="inner_content">
            <p>
                Контент- основная состовляющая сайта,
                поэтому  необходимо уделять ему много времени и внимания.
                Мы возьмем на себя эту непростую и громозкую задачу.
                Наша компания предоставляет услуги по заполнению и обновлению контента,
                написанию и рерайту статей,
                а также информационному сопровождению сайта.
            </p>
        </div>
    </div>
    <div class="service service_inner_ul">
        <h2>Чем мы наполняем сайт?</h2>
        <ul>
            <li><span></span><h3>Написание и рерайт статей</h3></li>
            <li><span></span><h3>Ведение информационных сайтов</h3></li>
            <li><span></span><h3>Наполнение сайта информацией</h3></li>
            <li><span></span><h3>Обновление контента сайта</h3></li>
        </ul>
    </div>
</div>
@endsection