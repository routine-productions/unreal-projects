@extends('layouts.master')

@section('head')
@endsection

@section('content')
<div class="header"><h1>Продвижение сайта</h1><span>Все что касается сайтов - это к нам</span></div>

<div class="services block">
    <div class="services_line">
        <div class="inner_services">
            <img src="/img/services/promote.jpg" alt="Продвижение сайта"/>
        </div>

        <div class="inner_content">
            <p>
                Чтобы о Вас говорили, нужно сначала рассказать о себе Самому.
                Необходимо подобрать нужные слова, которые вольются в сердце и
                душу человека, необходимо создать образ компании,
                которым будут пленены и восхищены массы.
                Необходимо сотворить идею, которая подчинит себе сотрудников
                и потребителей и направит их в одном направлении.
                Именно такие слова Мы сможем подобрать,
                именно такой образ мы высечем из холодной глыбы общественного мнения
                и именну такую идею мы вдохнем во все ваши проекты.
            </p>
        </div>
    </div>
    <div class="service service_inner_ul">
        <h2>Услуги по продвижению сайта</h2>
        <ul>
            <li><span></span><h3>Продвижение бренда</h3></li>
            <li><span></span><h3>Social media marketing (SMM)</h3></li>
            <li><span></span><h3>Search engine optimization (SEO)</h3></li>
            <li><span></span><h3>Public Relations (PR)</h3></li>
        </ul>
    </div>
    <div class="service service_inner_ul">
        <h2>План действия:</h2>
        <ul>
            <li><span></span>Вы ставите цель</li>
            <li><span></span>Мы достигаем успеха</li>
        </ul>
    </div>
</div>
@endsection