@extends('layouts.master')

@section('head')
@endsection

@section('content')
<div class="header"><h1>Улучшение сайта</h1><span>Все что касается сайтов - это к нам</span></div>

<div class="services block">
    <div class="services_line">
        <div class="inner_services">
            <img src="/img/services/remade.jpg" alt="Улучшение сайта"/>
        </div>

        <div class="inner_content">
    <!--        <h2>Улучшение сайта</h2>-->
            <p>
                Дизайн сайта давно устарел?
                Для реализации Ваших идей на сайте не хватает функционала или он слишком неудобен?
                Хочется многое сказать или показать миру, а количество страниц слишком мало?
                Или хочется просто сделать сайт новее, лучше и красивей?
                Тогда данная услуга именно для Вас, Мы поможем усовершенствовать Ваш сайт,
                добавить функционал, создать новые страницы или сделать редизайн.
            </p>
        </div>
    </div>
    <div class="service service_inner_ul">
        <h2>Как мы можем улучшить Ваш сайт?</h2>
        <ul>
            <li><span></span><h3>Расширить функционал сайта</h3></li>
            <li><span></span><h3>Добавить тематические страницы на сайт</h3></li>
            <li><span></span><h3>Разработать компоненты и модули</h3></li>
            <li><span></span><h3>Сделать редизайн сайт</h3></li>
        </ul>
    </div>
    <div class="service service_inner_ul">
        <h2>Что требуется от Вас?</h2>
        <ul>
            <li><span></span>Поделиться своими идеями, создав тех. задание</li>
            <li><span></span>Оценить получившийся результат</li>
            <li><span></span>Наслаждатся новым видом Вашего веб-детища</li>
        </ul>
    </div>

</div>
@endsection