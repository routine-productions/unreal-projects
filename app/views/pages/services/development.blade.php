@extends('layouts.master')

@section('head')
@endsection

@section('content')
<div class="header"><h1>Создание сайта</h1><span>Все что касается сайтов - это к нам</span></div>

<div class="services block">
    <div class="services_line">
        <div class="inner_services">
            <img src="/img/services/create.jpg" alt="Создание сайта"/>
        </div>

        <div class="inner_content">
    <!--<h2>Создание сайта</h2>-->
            <p>
                Реализация веб-проектов с нуля - это приоритет нашей компании.
                Мы создаем сайты любой направлености, сложности и функциональности.
                Сайт-визитка, интернет-магазин, информационный портал или "сайт под ключ" - мы создадим любой проект.
                С компанией Unrial Projects Ваши идеи реализуются  максимально быстро и качественно.
            </p>
        </div>
    </div>
    <div class="service service_inner_ul">
        <h2>Какие сайты мы можем сделать?</h2>
        <ul>
            <li><span></span><h3>Сайт визитка</h3></li>
            <li><span></span><h3>Информационный портал</h3></li>
            <li><span></span><h3>Интернет-магазин</h3></li>
            <li><span></span><h3>Любой Web-проект</h3></li>
        </ul>
    </div>

    <div class="service service_inner_ul">
        <h2>Oсновные этапы реализации проекта</h2>
        <ul>
            <li><span></span>Составление технического задания</li>
            <li><span></span>Создание дизайна</li>
            <li><span></span>Верстка сайта</li>
            <li><span></span>Реализация функционала</li>
            <li><span></span>Запуск сайта в Internet</li>
        </ul>
    </div>

</div>
@endsection