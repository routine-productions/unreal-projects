@extends('layouts.master')

@section('head')
@endsection

@section('content')
<div class="header"><h1>Починка сайта</h1><span>Все что касается сайтов - это к нам</span></div>

<div class="services block">
    <div class="services_line">
        <div class="inner_services">
            <img src="/img/services/tech.jpg" alt="Починка сайта"/>
        </div>

        <div class="inner_content">
    <!--        <h2>Починка сайта</h2>-->
            <p>
                Ошибки в работе сайта могут привести
                не только к ухудшению репутации среди пользователей,
                но и к материальным убыткам.
                Мы произведем быструю и качественную починку вашего сайта,
                исправим долгую загрузку страниц,
                и поможем восстановить сайт после взлома.
            </p>
        </div>
    </div>
    <div class="service service_inner_ul">
        <h2>Мы предоставляем следующие услуги:</h2>
        <ul>
            <li><span></span><h3>Исправление ошибок в работе сайта</h3></li>
            <li><span></span><h3>Техническое обслуживание сайта</h3></li>
            <li><span></span><h3>Восстановление сайта после взлома</h3></li>
            <li><span></span><h3>Оптимизация сайта</h3></li>
        </ul>
    </div>
    <div class="service service_inner_ul">
        <h2>Что требуется от Вас?</h2>
        <ul>
            <li><span></span>Поделиться своей проблемой с нами</li>
            <li><span></span>Наслаждаться работающим сайтом</li>
        </ul>
    </div>

</div>
@endsection