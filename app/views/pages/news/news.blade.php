@extends('layouts.master')

@section('head')
<link type="text/css" rel="stylesheet" href="css/history.css">
@endsection

@section('content')
<div class='history_coll'>
    @foreach($news as $new)
    <div class='event'>
        <div class="event_name">{{$new['event']}}</div>
        <div class="event_description">{{$new['description']}}</div>
        <div class="event_data">{{$new['data']}}</div>
        <div class="history_line">
            <div class="dot_left"></div>
            <div class="line"></div>
            <div class="dot_right"></div>
            <div class="year">{{$new['data']}}</div>
        </div>
    </div>

    @endforeach
</div>
@endsection