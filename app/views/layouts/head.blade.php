<title>{{isset($title)?$title:''}}</title>

<meta name="keywords" content="{{isset($keywords)?$keywords:''}}"/>
<meta name="description" content="{{isset($description)?$description:''}}"/>

<meta name="google-site-verification" content="GemE9a_2uia1fsirwBSQj9mCQ0xUYwz_EVKAt3hH3Uc" />
<meta name='wmail-verification' content='cbaf854e53e56df7' />
<meta name="msvalidate.01" content="BB8245714227CC4C6971802FF08D55B1" />

<link rel="shortcut icon" href="/img/fav.png" type="image/png">
<link type="text/css" rel="stylesheet" href="/css/reset.css">
<link type="text/css" rel="stylesheet" href="/css/index.css">
<link type="text/css" rel="stylesheet" href="/css/icons.css">
<link type="text/css" rel="stylesheet" href="/css/form.css">
<link type="text/css" rel="stylesheet" href="/css/media.css">

<script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="/js/jquery.cookie.js"></script>
<script src="/js/jquery.nicescroll.js"></script>
<script src="/js/jquery.modbox.js"></script>
<script src="/js/core.js"></script>
<script src="/js/iesettings.js"></script>
<script type="text/javascript" src="/js/jquery.scrollTo-min.js"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-51660403-1', 'unrealprojects.com');
    ga('send', 'pageview');
</script>

<meta charset="utf-8"/>