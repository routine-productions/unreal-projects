<!doctype HTML>
<html>
<head>
    @include('layouts.head')
    @yield('head')
</head>

<body>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter25182701 = new Ya.Metrika({
                    id: 25182701,
                    webvisor: true,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="//mc.yandex.ru/watch/25182701" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

<div class="load"><img src="/img/unreal-projects.png" alt="Unreal Projects"/></div>
<div class="grid"></div>
@include('layouts.menu')
<a href='/'>
    <div class="menu_icon"></div>
</a>


<div class="backtotop icons" alt="Вверх"></div>

@yield('in_body')

<div class="main_wrap">
    <div class="lightbox_wrap">
        <div class="lightbox_back"></div>
        <div class="lightbox"></div>
    </div>
    @yield('content')
</div>


</body>

</html>