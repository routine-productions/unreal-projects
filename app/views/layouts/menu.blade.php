<div class="info_line block">
    <div class="info_line_title">
        <a href="/"><img src="/img/unreal-project_minis.png"/></a>

        <div class="hidden_menu_wrap" style="display: none;">
            <div class="hidden_menu block" >


                <div class="menu_block menu_block_services">
                    <h4>Услуги</h4>
                    <ul>
                        <a href="/services/development"><li><span></span>Разработка</li></a>
                        <a href="/services/improvement"><li><span></span>Улучшение</li></a>
                        <a href="/services/servicing"><li><span></span>Починка</li></a>
                        <a href="/services/content"><li><span></span>Наполнение</li></a>
                        <a href="/services/promotion"><li><span></span>Продвижение</li></a>
                    </ul>
                </div>

                <div class="menu_block ">
                    <h4>Оставить заявку</h4>
                    <form id="order_services">
                        <input type="text" autocomplete="off" name="name" placeholder="ВАШЕ ИМЯ">
                        <input type="text" autocomplete="off" name="phone" placeholder="ВАШ ТЕЛЕФОН">
                        <input type="text" autocomplete="off" name="mail" placeholder="ВАШ E-MAIL">
                        <textarea name="task" placeholder="ВАША ЗАДАЧА"></textarea>
                        <input type="submit" value="ОТПРАВИТЬ ЗАЯВКУ">
                    </form>
                </div>


                <div class="menu_block ">
                    <h4>Компания</h4>
                    <ul>
                        <a href="/about"><li><span></span>О нас</li></a>
                        <a href="/history"><li><span></span>История</li></a>
                        {{--<a href="/news"><li><span></span>Новости</li></a>
                        <a href="/legends"><li><span></span>web легенды</li></a>--}}
                        <a href="/contacts"><li><span></span>Контакты</li></a>
                    </ul>
                </div>

                <div class="menu_block ">
                    <h4>Контакты</h4>
                    <ul>
                        <li><span class="email_icon"></span>EMAIL: OFFICE@UNREALPROJECTS.COM</li>
                        <li><span class="skype_icon"></span>SKYPE: UNREAL-PROJECTS</li>
                        <li><a href="http://vk.com/unreal_projects"><span class="vk_icon"></span>SOCIAL: VK.COM/UNREAL_PROJECTS</a></li>
                    </ul>
                </div>

                {{--
                <div class="menu_block ">
                    <h4>Последние проекты</h4>
                    <ul>
                        <a href="/sites/universe"><li><span></span>Инов. технологии</li></a>
                        <a href="/sites/florushka"><li><span></span>Флорушка</li></a>
                        <a href="/sites/azovsky"><li><span></span>ОРСП Азовский</li></a>
                        <a href="/sites/bunker"><li><span></span>Бункер -А-</li></a>
                    </ul>
                </div>
                --}}

                <div class="menu_contacts">
                    <div class="menu_contacts_phone">office@unrealprojects.com</div>
                    <div class="menu_contacts_mail">Copyright 2014 @ Web студия Unreal Projects</div>
                </div>
            </div>
        </div>
    </div>

    <div class="hidden_menu_wrap__our-projects" style="display: none;">
        <div class="hidden_menu__our-projects block" >
            <div class="menu_block menu_block_services">
                <h4>WEB-Легенды</h4>
                <ul>
                    <a href="/legends/social"><li><span></span>Социальные сети</li></a>
                    <a href="/legends/portals"><li><span></span>Информационные порталы</li></a>
                    <a href="/legends/education"><li><span></span>Образовательные сайты</li></a>
                    <a href="/legends/companies"><li><span></span>Мировые компании</li></a>
                    <a href="/legends/search"><li><span></span>Поисковые системы</li></a>
                    <a href="/legends/trade"><li><span></span>Торговые сети</li></a>
                    <a href="/legends/services"><li><span></span>Интернет сервисы</li></a>
                    <a href="/legends/torrents"><li><span></span>Торренты</li></a>

                </ul>
            </div>

        </div>
    </div>

    <div class="info_line_description"><h2>Разработка, улучшение и техническое обслуживание сайтов</h2></div>
    <a href="#"><div class="our-projects icons"></div></a>

</div>