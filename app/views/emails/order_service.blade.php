<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
    <hr>
    <p><i>Время:</i> {{ $datetime }}</p>
    <p><i>Имя:</i> {{ $name }}</p>
    <p><i>Телефон:</i> {{ $phone }}</p>
    <p><i>E-mail:</i> {{ $mail }}</p>
    <hr>
    <p><i>Сообщение:</i><br><br>  {{ $task }}</p>
</body>
</html>