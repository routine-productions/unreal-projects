@extends('layouts.master')

@section('head')
<link type="text/css" rel="stylesheet" href="css/legendary.css">
<style>
    .sites_wrap{
        width:{{$count*160}}px;
    }
</style>
@endsection

@section('content')
@endsection

@section('in_body')
<div class="hight-line"></div>
<div class="sites_wrap">
    @foreach ($sites as $site)
    <div class="website">
        <img src="/img/legendary/{{ ($site['logo']!='')?$site['logo']:'nophoto.jpg'}}" alt="{{ $site['name']}}"/>
        <div class="site_info">
            <a href="{{ $site['link']}}" target="_blank"><div class="site_info_name">{{$site['name']}}</div></a>
            <div class="site_info_description">{{$site['description']}}</div>
        </div>
    </div>
    @endforeach
    <div class="website">
        <img src="/img/legendary/add.jpg" alt="Доббавить сайт"/>
        <div class="site_info">
            <a href="Добавить" target="_blank"><div class="site_info_name add_site">Доббавить сайт</div></a>
            <div class="site_info_description">Если Вы хотите добавить сайт, жмите</div>
        </div>
    </div>
</div>
@endsection