@extends('layouts.master')

@section('head')
<link type="text/css" rel="stylesheet" href="css/legendary.css">
<style>
    .sites_wrap{
        width:{{$count*80}}px;
    }

</style>
<script>
    $(document).ready(function(){

        $('.add_site').click(function(){
            data='<div class="lightbox_block ">' +
                '<h4>Заявка на размешение сайта в список легендарных сайтов</h4>'+
                '<form class="lightbox_form">'+
                '<input type="text" autocomplete="off" name="name" placeholder="ВАШЕ ИМЯ">'+
                '<input type="text" autocomplete="off" name="phone" placeholder="ВАШ ТЕЛЕФОН">'+
                '<input type="text" autocomplete="off" name="mail" placeholder="ВАШ E-MAIL">'+
                '<textarea name="task" placeholder="ОСТАВЬТЕ ССЫЛКУ НА ВАШ САЙТ"></textarea>'+
                '<input type="submit" value="ОТПРАВИТЬ ЗАЯВКУ">'+
                '</form>'+
                '</div>';
            lightbox(data);
        });
    });
</script>
@endsection

@section('content')
@endsection

@section('in_body')
<div class="hight-line"></div>
<div class="sites_wrap">
    @foreach ($sites as $site)
    <div class="website small">
        <a href="http://{{ $site['link']}}/" target="_blank">
            <img src="/img/legendary/{{ ($site['logo']!='')?$site['logo']:'nophoto.jpg'}}" alt="{{ $site['name']}}"/>
        </a>
    </div>
    @endforeach
    <div class="website small add_site">
        <img src="/img/legendary/add.jpg" alt="Доббавить сайт"/>
    </div>
</div>
@endsection