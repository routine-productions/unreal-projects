@extends('layouts.master')

@section('head')
<link type="text/css" rel="stylesheet" href="css/legendary.css">
<script>
    $(document).ready(function(){
        $('.remove').click(function(){
            this_elem=this;
            $.ajax({
                url:'/admin/legends_delete',
                type:'POST',
                data:'name='+$('.list_line_name',$(this_elem).parent()).text(),
                success:function(){
                    $(this_elem).parent().remove();
                }
            });
            return false;
        });
    });
</script>
@endsection

@section('content')
<div class="header"><h1>Легендарные сайты</h1><span>Все что касается сайтов - это к нам</span></div>
<div class="services block">
    <h3 class="content_h3">Добавить сайт</h3>
    <form method="POST" enctype="multipart/form-data" class="form_body">
         <input type='text' name='name' placeholder="Название">
         <input type='text' name='link' placeholder="Ссылка"/>
         <select name='type'>
             <option value="0">Без категории</option>
             <option value="1">Социальные сети</option>
             <option value="2">Поисковые системы</option>
             <option value="3">Информационные порталы</option>
             <option value="4">Образовательные сайты</option>
             <option value="5">Интернет сервисы</option>
             <option value="6">Мировые компании</option>
         </select>
         <input type='file' name='photo' id="photo">
         <textarea name="description" placeholder="Описание"></textarea>

        <input type="submit"/>

    </form>
    <h3 class="content_h3">Список сайтов</h3>
    <div class="list">
        @foreach ($sites as $key => $site)
        <div class="list_line">
            <img class="list_line_logo" src="/img/legendary/{{ ($site['logo']!='')?$site['logo']:'nophoto.jpg'}}" alt="{{ $site['name']}}"/>
            <div class="list_line_elem list_line_type">{{ $types[$site['type']]}}</div>
            <div class="list_line_elem list_line_name">{{$site['name']}}</div>

            <div class="list_line_elem list_line_link">{{ $site['link']}}</div>
            <div class="list_line_elem list_line_description">{{$site['description']}}</div>
            <a href="#" class="remove">Удалить</a>
        </div>
        @endforeach
    </div>
</div>


@endsection
