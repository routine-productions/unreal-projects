<?php
return [
    'example' => [
        'title'=>'',
        'keywords'=>'',
        'description'=>''
    ],
    '/' => [
        'title'=>'Веб-студия Unreal Projects: разработка, улучшение, починка, наполнение и продвижение сайтов',
        'keywords'=>'вебстудия, веб студия, cоздание сайтов, разработка сайтов, продвижение, раскрутка, аудит сайта, сео, seo, контекстная реклама, поисковая оптимизация, создание интернет магазинов, интернет бизнес',
        'description'=>'Веб-студия Unreal Projects - создание сайтов, улучшение, продвижение, починка, доработка и наполнение сайтов по Украине и России'
    ],
    'contacts' => [
        'title'=>'Веб-студия Unreal Projects: Контакты',
        'keywords'=>'skype: unreal-projects, e-mail: office@unrealprojects.com',
        'description'=>'Контакты компании Unreal Projects'
    ],
    'about' => [
        'title'=>'Веб-студия Unreal Projects: О компании',
        'keywords'=>'выбрать веб-студию, создать сайт',
        'description'=>'Веб-студия Unreal Projects - создание сайтов, продвижение, починка, доработка и наполнение сайтов по Украине и России',
    ],
    'portfolio' => [
        'title'=>'Веб-студия Unreal Projects: Портфолио',
        'keywords'=>'skype: unreal-projects, e-mail: office@unrealprojects.com',
        'description'=>'Портфолио компании Unreal Projects'
    ],
    'history' => [
        'title'=>'История проекта',
        'keywords'=>'История проекта Unreal Projects',
        'description'=>'История проекта Unreal Projects'
    ],
    'news' => [
        'title'=>'Новости проекта',
        'keywords'=>'Новости проекта Unreal Projects',
        'description'=>'Новости проекта Unreal Projects'
    ],
    'services'=>[
        'development' => [
            'title'=>'Веб-студия Unreal Projects:Создание сайта',
            'keywords'=>'Создание уникальных сайтов, создание сайта визитки, создание информационного портала, создание интернет каталога, создание интернет магазина,создание сайта под ключ',
            'description'=>'Создание сайта Unreal Projects Украина,Россия'
        ],
        'improvement' => [
            'title'=>'Веб-студия Unreal Projects: Улучшение сайта',
            'keywords'=>'Расширение функционала сайта, добавление тематических страниц на сайт, разработка компонентов и модулей, редизайн сайта',
            'description'=>'Доработка и улучшение сайта, расширение функционала'
        ],
        'servicing' => [
            'title'=>'Веб-студия Unreal Projects: Починка сайта',
            'keywords'=>'Починка сайта, исправление ошибок в работе сайта, оптимизация сайта, восстановление сайта после взлома, техническое обслуживание сайта',
            'description'=>'Веб-студия Unreal Projects: исправление ошибок в работе сайта'
        ],
        'content' => [
            'title'=>'Веб-студия Unreal Projects: Наполнение сайта контентом',
            'keywords'=>'Обновление информации на сайте, наполнение сайта информацией, ведение информационных сайтов, написание и рерайт статей, content manager',
            'description'=>'Веб-студия Unreal Projects: создание, изменение и наполнение контента на сайте'
        ],
        'promotion' => [
            'title'=>'Веб-студия Unreal Projects: Продвижение сайта',
            'keywords'=>'Public Relations, PR, Search engine optimization, SEO, продвижение бренда, ',
            'description'=>'Веб-студия Unreal Projects: расскрутка и продвижение сайта и бренда с интернете'
        ],
    ],

    'sites'=>[
        'azovsky' => [
            'title'=>'ОРСП Азовский',
            'keywords'=>'',
            'description'=>''
        ],
        'university' => [
            'title'=>'Сайт для университета',
            'keywords'=>'',
            'description'=>''
        ],
        'florushka' => [
            'title'=>'Интернет каталог Флорушка',
            'keywords'=>'',
            'description'=>''
        ],
        'bunker' => [
            'title'=>'Мариупольский музыкальный клуб Бункер -А-',
            'keywords'=>'',
            'description'=>''
        ]
    ],

    'errors' => [
        'title'=>'Страница не существует',
        'keywords'=>'Страница не существует',
        'description'=>'Страница не существует'
    ],
];