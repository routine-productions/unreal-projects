-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Авг 30 2015 г., 14:51
-- Версия сервера: 5.5.29
-- Версия PHP: 5.5.12-2+deb.sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `webtech`
--

-- --------------------------------------------------------

--
-- Структура таблицы `history`
--

CREATE TABLE IF NOT EXISTS `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` varchar(255) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `history`
--

INSERT INTO `history` (`id`, `event`, `description`, `data`) VALUES
(2, '<a href=''http://vk.com/theearthproject'' alt=''The Earth''>Public Page - The Earth</a>', 'Кто мы? Куда идем? И главное:с какой целью? Мы долго искали ответы на эти вопросы в древних религиях и современных течениях. И результатом нашего поиска стал небольшой проект под названием The Earth . В нем были собраны эстетика и мудрость мировых учений,от дзен-буддизма до остроконечной готики. Насколько у нас это получилось- судить вам.', '2012-07-15'),
(3, '<a href=''http://unrealprojects.com/'' alt=''Unreal Projects''>Web Studio - Unreal Projects</a>', 'Падкость на идеи развивалась. И из проекта наблюдающего за идеями со стороны, мы решили создать проект, который бы сам изобретал и воплощал идеи в жизнь. Первым шагом к реализации этого плана было создание веб-студии, как инструмента позволяющего реализовывать новые и смелые идеи, неограниченные ни географическими масштабами, ни социальными устоями, ни законами физики в целом. Итак, разрешите представить Вашему вниманию наш нереальный проект - веб-студию "Unreal Project".', '2014-06-01');

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` varchar(255) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `event`, `description`, `data`) VALUES
(1, 'Мы открываемся', 'Мы открываемся', '2014-06-01');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`) VALUES
(1, 'root', '$2y$10$.oB/XkDbfFXLZIGeINGtDO6EcMLozxkQ.G7mryQx5NLVkK9pymD9u');

-- --------------------------------------------------------

--
-- Структура таблицы `web_legends`
--

CREATE TABLE IF NOT EXISTS `web_legends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `link` varchar(56) CHARACTER SET utf8 NOT NULL,
  `logo` varchar(56) CHARACTER SET utf8 NOT NULL,
  `type` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Дамп данных таблицы `web_legends`
--

INSERT INTO `web_legends` (`id`, `name`, `description`, `link`, `logo`, `type`) VALUES
(24, 'Вконтакте', 'Лозунг: «современный, быстрый и эстетичный способ общения в сети».', 'vk.com', 'Вконтакте.jpg', 1),
(25, 'facebook', 'Крупнейшая социальная сеть в мире.', 'facebook.com', 'facebook.jpg', 1),
(43, 'Одноклассники', '«Однокла́ссники» — мультиязычная социальная сеть, используемая для поиска одноклассников, однокурсников, бывших выпускников', 'odnoklassniki.ru', 'Одноклассники.jpg', 1),
(46, 'twitter', 'Твиттер — сервис для публичного обмена короткими  сообщениями.', 'twitter.com', 'twitter.jpg', 1),
(47, 'Google+', 'Сервис предоставляет возможность общения через Интернет с помощью специальных компонентов: Круги, Темы, ВидеоВстречи, Мобильная версия', 'plus.google.com', 'Google+.jpg', 1),
(48, 'Sina Weibo', 'Sina Weibo (кит. 新浪微博) — китайский сервис микроблого.', 'weibo.com', 'Sina Weibo.jpg', 1),
(50, 'Фотострана', 'Cоциально-развлекательная сеть. Возможность знакомиться, общаться, играть в онлайн игры, завести виртуального питомца.', 'http://fotostrana.ru/', 'Фотострана.jpg', 1),
(51, 'LiveJournal', 'Блог-платформа для ведения онлайн-дневников, а также отдельный персональный блог', 'livejournal.com', 'LiveJournal.jpg', 1),
(52, 'Badoo', 'Социальная сеть знакомств, поддерживающая множество языков и работающая с пользователями всех стран мира.', 'badoo.com', 'Badoo.jpg', 1),
(53, 'Myspace', 'Международная социальная сеть, в которой представлена возможность создания сообществ по интересам, персональных профилей, ведение блогов и т.д.', 'https://myspace.com', 'Myspace.jpg', 1),
(54, 'Skyrock', 'Самая популярная социальная сеть Франции.', 'Skyrock.com', 'Skyrock.jpg', 1),
(55, 'Orkut', 'Помощь в нахождении новых друзей и упрочнение существующих связей. ', 'http://www.orkut.com/', 'Orkut.jpg', 1),
(56, 'МирТесен', 'Первая социальная сеть на карте.', 'mirtesen.ru/', 'МирТесен.jpg', 1),
(57, 'ВКругуДрузей', 'Русскоязычная социальная сеть.', 'vkrugudruzei.ru', 'ВКругуДрузей.jpg', 1),
(58, 'ВСети', 'Сервис для общения и поиска знакомых.', 'vseti.by', 'ВСети.jpg', 1),
(59, 'YouTube', 'Сервис, предоставляющий услуги видеохостинга.', 'YouTube.com', 'YouTube.jpg', 1),
(60, 'Мой Мир', 'Социальная сеть, формирующая профили пользователей на основе их информации в других порталах проекта Mail.ru.', 'my.mail.ru', 'Мой Мир.jpg', 1),
(61, 'Instagram', 'Приложение, совмещающий в себе фоторедактор и весьма популярную социальную сеть.', 'instagram.com', 'Instagram.jpg', 1),
(62, 'Hi5', 'Социальная сеть,  активно развивающая направление социального гейминга', 'www.hi5.com/', 'Hi5.jpg', 1),
(63, 'QQ', 'Программа для мгновенного обмена сообщениями, имеющая огромную популярность в Китае.', 'www.imqq.com', 'QQ.jpg', 1),
(64, 'RenRen', 'Популярная китайская социальная сеть, популярность сервис получил в китайской студенческой среде.', 'www.renren.com', 'RenRen.jpg', 1),
(65, 'Мой круг', 'Сервис для поиска работы и сотрудников, размещения услуг и установления деловых связей.', 'moikrug.ru', 'Мой круг.jpg', 1),
(66, 'Flickr', 'Социальная сеть с фотохостингом и видеохостингом.', 'https://www.flickr.com/', 'Flickr.jpg', 1),
(67, 'Placeword', 'Современная социальная сеть с революционным подходом к реализации принципов организации и управления различными типами сообществ. ', 'Placeword.com', 'Placeword.jpg', 1),
(68, 'Xbeee', 'Совмещение Социальной сети, Блогосферы и СМИ.', 'xbeee.com', 'Xbeee.jpg', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
