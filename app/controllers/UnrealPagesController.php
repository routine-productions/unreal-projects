<?php

class UnrealPagesController extends BaseController
{

    /*
     * Вывод главной страницы
     * @return mixed
     */

    public function actionHome()
    {
        return View::make('pages.main', Config::get('headers./'));
    }

    public function actionPortfolio()
    {
        return View::make('pages.static.portfolio', Config::get('headers.portfolio'));
    }

    /**
     * Вывод статических страниц сайтов
     */
    public function actionSites($page = 'azovsky')
    {
        if (View::exists('pages.sites.' . $page)) {
            return View::make('pages.sites.' . $page, Config::get('headers.sites.' . $page));
        } else {
            return Redirect::to('/errors');
        }
    }

    /**
     * Вывод статических страниц услуг
     */
    public function actionServices($page = 'development')
    {
        if (View::exists('pages.services.' . $page)) {
            return View::make('pages.services.' . $page, Config::get('headers.services.' . $page));
        } else {
            return Redirect::to('/errors');
        }
    }

    /**
     * История развития компании
     */
    public function actionHistory()
    {
        $data = [
                'events' => History::orderBy('data', 'DESC')->get()->toArray(),

            ] + Config::get('headers.history');

        return View::make('pages.history.history', $data);
    }

    /**
     * @return mixed
     *
     * Новости компании
     */
    public function actionNews()
    {
        $data = [
                'news' => News::orderBy('data', 'DESC')->get()->toArray(),
            ] + Config::get('headers.news');
        return View::make('pages.news.news', $data);
    }


    /**
     *  Контакты
     */
    public function actionContacts()
    {
        return View::make('pages.static.contacts', Config::get('headers.contacts'));
    }


    /**
     * О компании
     */
    public function actionAbout()
    {
        return View::make('pages.static.about', Config::get('headers.about'));
    }

    /*
     * Страницы не существует
     */
    public function actionErrors()
    {
        return View::make('pages.error', Config::get('headers.errors'));
    }

}