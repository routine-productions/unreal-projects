<?php

class UnrealMailController extends BaseController {

    /*
     * Отправка сообщений
     */
    public function actionOrderServices()
	{
        $data = [
                    'name' =>  Input::get('name')?:'',
                    'phone' => Input::get('phone')?:'',
                    'mail' => Input::get('mail')?:'',
                    'task' => Input::get('task')?:'',
                    'datetime' => strval(date('Y-m-d H:i:s'))
        ];
        if($data['name'] || $data['phone'] || $data['mail'] || $data['task']){
            Mail::send('emails.order_service', $data, function($message)
            {
                $message->from('olegblud@gmail.com', 'Unreal Projects');
                $message->to('office@unrealprojects.com', 'office@unrealprojects.com')->subject('Новый заказ');
            });
            echo json_encode(['sended'=>'1']);
        }else{
            echo json_encode(['sended'=>'2']);
        }
	}

}
