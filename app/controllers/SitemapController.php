<?php

class SitemapController extends BaseController {
    public function actionSitemap(){

        // create new sitemap object
        $sitemap = App::make("sitemap");

        // $sitemap->setCache('laravel.sitemap', 3600);

        $sitemap->add(URL::to('/'), '2014-06-01T12:30:00+02:00', '1.0', 'monthly');

        $sitemap->add(URL::to('/services/development'), '2014-06-01T12:30:00+02:00', '0.9', 'monthly');
        $sitemap->add(URL::to('/services/improvement'), '2014-06-01T12:30:00+02:00', '0.9', 'monthly');
        $sitemap->add(URL::to('/services/servicing'), '2014-06-01T12:30:00+02:00', '0.9', 'monthly');
        $sitemap->add(URL::to('/services/content'), '2014-06-01T12:30:00+02:00', '0.9', 'monthly');
        $sitemap->add(URL::to('/services/content'), '2014-06-01T12:30:00+02:00', '0.9', 'monthly');

        $sitemap->add(URL::to('/about'), '2014-06-01T12:30:00+02:00', '0.8', 'monthly');
        $sitemap->add(URL::to('/contacts'), '2014-06-01T12:30:00+02:00', '0.8', 'monthly');
        $sitemap->add(URL::to('/history'), '2014-06-01T12:30:00+02:00', '0.8', 'monthly');

        return $sitemap->render('xml');
    }

}