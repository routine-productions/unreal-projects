<?php

class RegController extends Controller{

    public function actionLogin(){
        echo Hash::make('987975');
        return View::make('admin.reg.login',Config::get('headers./'));
    }

    public function actionAuth(){
        if (Auth::attempt(array('login' => Input::get('login'), 'password' => Input::get('password')))) {
            return Redirect::intended('admin/legends');
        }else{
            return Redirect::to('/login');
        }
    }
    public function actionLogout(){
        Auth::logout();
        return Redirect::to('/login');
    }
} 