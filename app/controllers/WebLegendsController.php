<?php

class WebLegendsController extends BaseController {

    /*
     * Вывод всех сайтов
     * @return mixed
     */

    public function actionBigLegends()
	{
       $legends=$this->shuffle_assoc(
           WebLegends::all()->toArray()
       );

       $data = [
                    'sites'=>$legends,
                    'title'=>'Легендарные сайты',
                    'count'=>ceil(sqrt(count($legends)+1))
                ];
       return View::make('web_legends.small_legends',$data);
	}

    public function shuffle_assoc($list) {
        if (!is_array($list)) return $list;

        $keys = array_keys($list);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $list[$key];
        }
        return $random;
    }

    public function actionAddLegends(){
        $name = Input::get('name');
        $type = Input::get('type');
        $link = Input::get('link');
        if(strlen($name)>=2 && !empty($type) && !empty($link)){
            WebLegends::saveLegend();
        }

        $types = [
                    0=>'Без категории',
                    1=>'Социальные сети',
                    2=>'Поисковые системы',
                    3=>'Информационные порталы',
                    4=>'Образовательные сайты',
                    5=>'Интернет сервисы',
                    6=>'Мировые компании'
                ];

        $legends = WebLegends::orderBy('id','DESC')->get()->toArray();
        $data = [
            'sites'=>$legends,
            'types'=>$types
        ];

        return View::make('web_legends.add_legends',$data);
    }

    public function actionDeleteLegends(){
        WebLegends::where('name', '=', Input::get('name'))->delete();
    }

}
