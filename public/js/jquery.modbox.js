(function($){
    var methods = {
        /**
         * Создание лайтбокса
         */
        open: function(content,options){
            /**
             * Настройки
             */
            var settings = $.extend( {
                'event' : 'click',
                'modbox_style': {
                    width: '100%',
                    position: 'fixed',
                    height: '100%',
                    top: '0px',
                    bottom: '0px',
                    left: '0px',
                    right: '0px',
                    display:'none',
                    'z-index': '9999',
                    background: 'rgba(0, 0, 0, 0.67)'
                },
                'modbox_back':{
                    width: '100%',
                    position: 'absolute',
                    height: '100%'
                },
                'modbox_content':{}
            }, options);


                $(this).bind(settings['event']+'.modbox',{obj:this,content:content,settings:settings},methods.show);
                $(window).bind('resize.modbox',function(){
                    $('.modbox_content').css('marginTop',($(window).height()-$('.modbox_content').outerHeight())/2);
                    console.log($('.modbox_content').outerHeight());
                });
        },
        /**
         * Закрыть лайтбокс
         */
        close: function(e){
            $('.modbox').fadeOut(1000,function(){
                $('.modbox').remove();
            });
        },
        /**
         * Показать лайтбокс
         */
        show: function(e){
            $('body').append('<div class="modbox">' +
                                '<div class="modbox_back"></div>' +
                                '<div class="modbox_content">' +
                                        e.data.content +
                                '</div>'+
                             '</div>');

            $('.modbox').css(e.data.settings.modbox_style);
            $('.modbox_back').css(e.data.settings.modbox_back);
            $('.modbox_content').css(e.data.settings.modbox_content);

            console.log($('.modbox_content').innerHeight());
            $('.modbox_content').css('marginTop',($(window).height()+$('.modbox_content').height())/2);
            $('.modbox').fadeIn({start:function(){
                $('.modbox_content').css('marginTop',($(window).height()-$('.modbox_content').outerHeight())/2);
            },duration:1000});
            $('.modbox').bind('click.modbox',methods.close);
        }
    };

    $.fn.modbox = function(method){
        if(methods['method'] == 'close', methods['method'] == 'show'){
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        }else{
            return methods.open.apply( this, arguments );
        }
    }
})(jQuery);