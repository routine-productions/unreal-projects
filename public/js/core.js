(function($){
$(document).ready(function(){
    $('.load img').css('marginTop',$(window).height()/2-45);

    //Загрузка фона
   if($.cookie('atFirst')){
       $('.load').delay(1000).fadeOut(1000);
       $.cookie('atFirst', 'false', { expires: 1 });
   }else{
       $('.load').remove();
   }

   nicescrollObj=null;
   function nicescroll(){
       nicescrollObj=$("html").niceScroll({cursorcolor:"black",cursorwidth:'10',cursorborderradius:'0',cursorborder:'none',zindex:"9999"});
   }
    nicescroll();



//    $('.info_line_title img').hover(function(){
//        $(this).animate({'opacity':0.7},500,'easeOutCubic').animate({'opacity':1},150,'easeOutCubic');
//    });

    $('.icon_round,.icons').hover(function(){
        $(this).animate({ borderSpacing: '+=360' }, {
            step: function(now,fx) {
                $(this).css('-webkit-transform','rotate('+now+'deg)');
                $(this).css('-moz-transform','rotate('+now+'deg)');
                $(this).css('transform','rotate('+now+'deg)');
            },
            duration:700
        },'easeOutQuint');
    },function(){
    });


    $('.service ul li,.menu_block ul li').hover(function(){
        $('span',this).animate({ borderSpacing: '+=360' }, {
            step: function(now,fx) {
                $(this).css('-webkit-transform','rotate('+now+'deg)');
                $(this).css('-moz-transform','rotate('+now+'deg)');
                $(this).css('transform','rotate('+now+'deg)');
            },
            duration:600
        },'easeOutQuint');

    },function(){

    });

    // Выпадающее меню
    $('.info_line_title,.menu_icon').hover(function(){
        $('.hidden_menu_wrap').show();
    },function(){
        $('.hidden_menu_wrap').hide();
    });

    // Выпадающее меню
    $('.our-projects,.hidden_menu_wrap__our-projects,.hidden_menu__our-projects .block').hover(function(){
        $('.hidden_menu_wrap__our-projects').show();
    },function(){
        $('.hidden_menu_wrap__our-projects').hide();
    });

    $(window).on('scroll',function(){
        if($(window).scrollTop()<300){
            $('.backtotop').attr('style','background-position:-240px -60px !important');
        }else{
            $('.backtotop').attr('style','background-position:-210px -60px !important');
        }

        if($(window).scrollTop()>35){
            $('.hidden_menu_wrap').css('top','-6px');
        }else{

            $('.hidden_menu_wrap').css('top','36px');
        }
    });

    $('.website').hover(function(){
        $('.site_info',this).animate({'height':'80px'},200,'easeInCubic');
    },function(){
        $('.site_info',this).animate({'height':'0px'},100,'easeInCubic');
    });

    $('.form').hover(function(){
        $('.hidden_form_wrap').show();
    },function(){
        $('.hidden_form_wrap').hide();
    });

    /**
     * Отправка данных с формочки заявки
     */
    $(document).on('click',
                    '#order_services input[type=submit],' +
                    '#order_services_contacts input[type=submit],' +
                    '.lightbox_form input[type=submit],' +
                    '#order_services_front input[type=submit]',
        function(){

        data=$($(this).parent()).serialize();
        this_click=this;

        if($('input[name=mail]',$(this).parent()).val() ||
            $('input[name=phone]',$(this).parent()).val() ||
            $('textarea[name=task]',$(this).parent()).val()){
            $.ajax({
                type: "POST",
                url: "/mail/orders_services",
                dataType: "json",
                data: data,
                success: function(data){
                    if(data['sended']==1){
                        $('input[type=text] , textarea[name=task]',$(this_click).parent()).val('');
                        $('input[type=submit]',$(this_click).parent()).after('<div class="thank_for_message">Ваша заявка отправлена.</div>');
                        $('.thank_for_message',$(this_click).parent()).delay(4000).fadeOut(500);
                    }else{
                        $( $(this).parent()).effect('bounce',{ times: 10 ,distance: 25} ,800);
                    }
                }
            });
        }else{
            $($(this).parent()).effect('bounce',{ times: 10 ,distance: 25} ,800);
        }
        return false;
    });

    $('.backtotop').click(function() {
        if($(window).scrollTop()<300){
            $.scrollTo(($('html').height()-$(window).height()),1500,{easing:'easeOutBounce'} );
        }
        else{
            $.scrollTo('0px',1500,{easing:'easeOutBounce'} );
        }
    });
/*
    lightbox = function lightbox(content){
        if(content==undefined){
            content='';
        }else{
            $(".lightbox").html(content);
            nicescrollObj.remove();
        }
        $(".lightbox_wrap").fadeToggle(400);



        $('.icon_round,span').stop(true,true);
        $('.icon_round,span').css('-webkit-transform','none');
    }*/



        data='<div class="lightbox_block ">' +
            '<h4>Оставить заявку</h4>'+
            '<form class="lightbox_form">'+
            '<input type="text" autocomplete="off" name="name" placeholder="ВАШЕ ИМЯ">'+
            '<input type="text" autocomplete="off" name="phone" placeholder="ВАШ ТЕЛЕФОН">'+
            '<input type="text" autocomplete="off" name="mail" placeholder="ВАШ E-MAIL">'+
            '<textarea name="task" placeholder="ОПИШИТЕ ЗАДАЧУ"></textarea>'+
            '<input type="submit" value="ОТПРАВИТЬ ЗАЯВКУ">'+
            '</form>'+
            '</div>';

    $('.service li').modbox(data);

        data='<div class="lightbox_block ">' +
            '<h4>Оставить заявку</h4>'+
            '<form class="lightbox_form">'+
            '<input type="text" autocomplete="off" name="name" placeholder="ВАШЕ ИМЯ">'+
            '<input type="text" autocomplete="off" name="phone" placeholder="ВАШ ТЕЛЕФОН">'+
            '<input type="text" autocomplete="off" name="mail" placeholder="ВАШ E-MAIL">'+
            '<textarea name="task" placeholder="ОПИШИТЕ ЗАДАЧУ"></textarea>'+
            '<input type="submit" value="ОТПРАВИТЬ ЗАЯВКУ">'+
            '</form>';

    $('.icon_round').modbox(data);

/*
    // Вращающиеся шары
    $('.cube img').each(function(key,val){
        size=50+Math.random()*100;
        if(key%2==0){
            $(val).css({
                'top':$(window).height()*Math.random(),
                'left':($(window).width()-1040)/2*Math.random()

            });

            rand=Math.random();
            $(val).delay(5000*rand)
                .animate({'opacity':rand},2000,'linear')
                .delay(5000*rand)
                .animate({'left':'+='+rand*100},4000,'easeInOutElastic')
                .delay(2000*rand)
                .effect('bounce',{ times: 8 ,distance: 30} ,1800)
                .animate({'left':'-='+rand*100},4000,'easeInOutElastic')
                .delay(2000*rand)
                .effect('bounce',{ times: 8 ,distance: 30} ,1800)
                .animate({'left':'+='+rand*100},4000,'easeInOutElastic')
                .delay(2000*rand)
                .effect('bounce',{ times: 8 ,distance: 30} ,1800)
                .animate({'left':'-='+rand*100},4000,'easeInOutElastic')
                .delay(2000*rand)
                .effect('bounce',{ times: 8 ,distance: 30} ,1800)
                .animate({'opacity':'0'},3000,'easeInOutElastic');
        }else{
            $(val).css({
                'top':$(window).height()*Math.random()+100,
                'right':($(window).width()-1040)/2*Math.random()
            });
            rand=Math.random();
            $(val).delay(500)
                .animate({'opacity':rand},4000,'linear')
                .delay(5000*rand)
                .animate({'right':'+='+rand*100},4000,'easeInOutElastic')
                .delay(000)
                .effect('bounce',{ times: 8 ,distance: 30} ,1800)
                .animate({'right':'-='+rand*100},4000,'easeInOutElastic')
                .delay(2000)
                .effect('bounce',{ times: 8 ,distance: 30} ,1800)
                .animate({'right':'+='+rand*100},4000,'easeInOutElastic')
                .delay(2000)
                .effect('bounce',{ times: 8 ,distance: 30} ,1800)
                .animate({'right':'-='+rand*100},4000,'easeInOutElastic')
                .delay(2000)
                .effect('bounce',{ times: 8 ,distance: 30} ,1800)
                .animate({'opacity':'0'},4000,'easeInOutElastic');
        }
    });*/

});
})(jQuery);